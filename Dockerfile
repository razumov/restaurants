FROM python:3-alpine
RUN pip install requests
ENV GOCROND_VERSION 0.6.1
RUN set -ex && \
	apk add --update curl tzdata && \
	curl -fsSL -o /usr/local/bin/go-crond https://github.com/webdevops/go-crond/releases/download/$GOCROND_VERSION/go-crond-64-linux && \
	chmod +x /usr/local/bin/go-crond && \
	apk del curl && \
	rm -rf /var/cache/apk/*
COPY novae.py crontab /srv/
# gocrond doesn't switch users in unpriv'd mode, but we still need to
# specify one so it doesn't expect one inside the crontab itself
CMD ["/usr/local/bin/go-crond", "-v", "--allow-unprivileged", "--no-auto", "johndoe:/srv/crontab"]
