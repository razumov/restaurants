import re
import traceback
from datetime import date, datetime, timedelta

import os
import requests
import sys


try:
    # mattermost webhook url
    MMHOOK = os.environ['MATTERMOST_HOOK']
    # bot owner for errors
    OWNER = os.environ['BOT_OWNER']
    # mattermost channel
    CHANNEL = os.environ['MATTERMOST_CHANNEL']
    # https://mynovae.ch access token
    TOKEN = os.environ['NOVAE_TOKEN']
except KeyError as exc:
    print(f'Required environment variable not set: {exc}')
    sys.exit(2)

# colors for the different restaurants
COLORS = {1: '#22dd77', 2: '#1188ee', 3: '#aa11ee'}


def get_dishes(menus):
    dishes = []
    for item in menus:
        type_ = item['model']['title'].strip()
        title = (item['title'].get('fr') or item['title'].get('en')).strip().split('\n')[0].strip()
        dishes.append((type_, title))
    return sorted(dishes)


def mattermost_post_menus(day, menus):
    day = day.strftime('%A, %d %B %Y')
    fallback = []
    for num, menu in menus.items():
        if not menu:
            continue
        fallback.append(f'Dishes in R{num}:')
        for type_, name in menu:
            fallback.append(f'{type_}: {name}')
        fallback.append('')
    payload = {
        'channel': CHANNEL,
        'username': 'CERN Restaurants',
        'text': f'#### Menus for {day}',
        'attachments': [{
            'title': f'Restaurant {num}',
            'fallback': '\n'.join(fallback),
            'text': '' if menu else 'No menu available',
            'fields': [{'title': type_, 'value': dish, 'short': True} for type_, dish in menu] if menu else [],
            'color': COLORS[num],
        } for num, menu in menus.items()]
    }
    requests.post(MMHOOK, json=payload).raise_for_status()


def mattermost_post_error(title, message=None):
    if message is None:
        message = traceback.format_exc().strip()
    payload = {
        'channel': f'@{OWNER}',
        'username': 'CERN Restaurants',
        'attachments': [{
            'title': title,
            'fallback': message,
            'text': f'```\n{message}\n```',
            'color': '#FF5000'
        }]
    }
    requests.post(MMHOOK, json=payload).raise_for_status()


def main():
    day = date.today()
    if datetime.now().hour > 14:
        day += timedelta(days=1)
    url = f'https://api.mynovae.ch/en/api/connected/menu/{day.isoformat()}?empty=1&public=1'
    try:
        response = requests.get(url, headers={'Authorization': f'Bearer {TOKEN}'})
        response.raise_for_status()
        data = response.json()
    except Exception:
        mattermost_post_error('Could not fetch menus')
        return
    menus = {}
    for restaurant_data in data:
        try:
            if 'CERN' not in restaurant_data['client']['name']:
                continue
            match = re.search(r'Self Restaurant R(\d)', restaurant_data['name'])
            if not match:
                continue
        except Exception:
            mattermost_post_error('Invalid restaurant data')
            continue
        restaurant_num = int(match.group(1))
        if 'menus' not in restaurant_data:
            menus[restaurant_num] = None
            continue
        try:
            menus[restaurant_num] = get_dishes(restaurant_data['menus'])
        except Exception:
            mattermost_post_error('Could not extract dishes')
            continue
    # send errors if we don't have data for the 3 CERN restaurants
    if set(menus) != {1, 2, 3}:
        mattermost_post_error('Sanity check failed', f'Invalid restaurant list: {set(menus)}')
        # XXX: keep going, missing data is better than no data, especially until we know
        #      how they expose days where the restaurant is closed.
    # send the menus to the channel
    try:
        mattermost_post_menus(day, menus)
        sys.exit(0)
    except Exception:
        mattermost_post_error('Could not post menus')
        sys.exit(1)


if __name__ == '__main__':
    main()
